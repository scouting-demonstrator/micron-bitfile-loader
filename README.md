**Micron Bitfile Loader**

Compile with compile with g++ -o MicronBitfileLoader MicronBitfileLoader.cc -std=c++11 -lmicron -Llibmicron -lpthread -lcrypto

Run with: ./MicronBitfileLoader "pathtofile"

Compatible with picocomputing-2021.2.7.
