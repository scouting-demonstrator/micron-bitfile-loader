#!/bin/bash

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
BUILD_DIR="${DIR}/micron-bitfile-loader_rpmbuild"
cd $DIR
echo "Directory $DIR"

RAWVER=$(git describe --tags || echo 'v0.0.0')
VER=$(echo ${RAWVER} | sed 's/^v//' | awk '{split($0,a,"-"); print a[1]}') # If git describe fails, we set a default version number.
REL=$(git rev-list HEAD | wc -l | tr -d ' ')
TIM="$(date -u +"%F %T %Z")"
PCR="scouter" #"`logname`"

echo -n "{" >info.json
echo -n "\"version\": \"$VER\", " >>info.json
echo -n "\"release\": \"$REL\", " >>info.json
echo -n "\"time\": \"$TIM\", " >>info.json
echo -n "\"packager\": \"$PCR\"" >>info.json
echo -n "}" >>info.json

echo "Version $VER, release $REL"

rm -fR ${BUILD_DIR}/SOURCES
mkdir -p ${BUILD_DIR}/SOURCES

pwd
mkdir -p ${BUILD_DIR}/SOURCES/micron-bitfile-loader
cp -R ${DIR}/src ${BUILD_DIR}/SOURCES/micron-bitfile-loader
tar cf ${BUILD_DIR}/SOURCES/micron-bitfile-loader.tar -C ${BUILD_DIR}/SOURCES micron-bitfile-loader
rm -fR ${BUILD_DIR}/SOURCES/micron-bitfile-loader

RPM_OPTS=(--define "_topdir $BUILD_DIR" --define "_version $VER" --define "_release $REL" --define "_packager $PCR")
rpmbuild "${RPM_OPTS[@]}" -bb package/package.spec
ls ${BUILD_DIR}/RPMS/x86_64/
ls ${BUILD_DIR}/RPMS/x86_64/package/
cp ${BUILD_DIR}/RPMS/x86_64/micron-bitfile-loader-* .

