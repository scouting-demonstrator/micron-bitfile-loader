#ifndef MICRON_BITFILE_LOADER_H
#define MICRON_BITFILE_LOADER_H

#include <micron_dma.h>
#include <iostream>
#include <stdio.h>
#include <string>

int main(int argc, char *argv[]){
 micron_private *pico_;
 std::string bitFileName = argv[1];
 std::cout << "Loading FPGA with '" << bitFileName << "' ...";
 int err = micron_run_bit_file(bitFileName.c_str(), &pico_);
 if(err < 0){
   std::cout << "load bitfile failed with error " << err << std::endl;
   return -1;
 }
 std::cout << "load bitfile successful, returned " << err << std::endl;
 return err;
}

#endif
